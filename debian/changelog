ruby-jquery-atwho-rails (1.5.4+dfsg-3) unstable; urgency=medium

  * Copy assets during build instead of symlink (Closes: #1008468)
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Mon, 23 Jan 2023 17:09:08 +0530

ruby-jquery-atwho-rails (1.5.4+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Update standards version to 4.4.1, no changes needed.

  [ Daniel Leidert ]
  * d/control: Add Rules-Requires-Root field.
    (Build-Depends): Bump debhelper to version 13. Add javascript packages.
    (Standards-Version): Bump to 4.6.0.
    (Homepage): Update URL.
    (Depends): Remove interpreters and use ${ruby:Depends}.
  * d/copyright: Add Upstream-Contact field.
    (Source): Update URL.
    (Copyright): Add team.
  * d/install: Remove file. Not necessary anymore.
  * d/links: Ditto.
  * d/rules: Use gem installation layout. Clean file. Create links here.
  * d/watch: Bump to version 4.
  * d/engine-root.patch: Remove obsolete patch.
  * d/patches/series: Ditto.
  * d/tests/control: Mark smoke-test as superficial.
  * d/tests/smoke-test: Fix to run with current Rails.
  * d/upstream/metadata: Add file.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 08 Dec 2021 10:01:11 +0100

ruby-jquery-atwho-rails (1.5.4+dfsg-1) unstable; urgency=medium

  * New upstream version 1.5.4+dfsg
  * Build Depends: Replace ruby-railties + ruby-sprockets-rails with ruby-rails
  * Remove unnecessary patch to require sprokets in tests
  * Switch to packaged versions of jquery.atwho.js and jquery.caret.js
  * Exclude embedded jquery.atwho.js and jquery.caret.js from source
  * Add a smoke test as autopkgtest

 -- Pirate Praveen <praveen@debian.org>  Thu, 21 Feb 2019 18:57:16 +0530

ruby-jquery-atwho-rails (1.3.2-3) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files
  * Use new default gem2deb Rakefile to run the tests

  [ Pirate Praveen ]
  * Require sprockets/railties in spec_helper.rb (Closes: #918611)
  * Add ruby-aprockets-rails as build dependency
  * Use salsa.debian.org in Vcs-* fields
  * Bump debhelper compatibility level to 11
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Move debian/watch to gemwatch.debian.net

 -- Pirate Praveen <praveen@debian.org>  Tue, 19 Feb 2019 15:51:38 +0530

ruby-jquery-atwho-rails (1.3.2-2) unstable; urgency=medium

  * Install assets and set rails engine root (Closes: #803693)

 -- Pirate Praveen <praveen@debian.org>  Mon, 02 Nov 2015 04:19:34 +0530

ruby-jquery-atwho-rails (1.3.2-1) unstable; urgency=medium

  * Initial release (Closes: #791585)

 -- Pirate Praveen <praveen@debian.org>  Tue, 07 Jul 2015 12:11:12 +0530
